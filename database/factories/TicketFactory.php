<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Ramsey\Uuid\Type\Decimal;

class TicketFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'purchase_date' => $this->faker->dateTime(),
            'price' => rand(7000, 50000),
            'discount' => rand(0, 100),
            'stage_id' => rand(1, 6),
            'user_id' => rand(1, 10)
        ];
    }
}
