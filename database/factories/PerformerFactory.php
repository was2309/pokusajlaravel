<?php

namespace Database\Factories;

use App\Models\Performer;
use Illuminate\Database\Eloquent\Factories\Factory;

class PerformerFactory extends Factory
{

    protected $model = Performer::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'surname' => $this->faker->lastName,
            'nick' => $this->faker->name,
            'music_genre' => $this->faker->name
        ];
    }
}
