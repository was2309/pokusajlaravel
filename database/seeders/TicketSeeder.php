<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tickets')->insert([[
            'purchase_date' => Carbon::create('2022', '7', '20'),
            'price' => 8900,
            'discount' => 0,
            'stage_id' => rand(1, 6),
            'user_id' => rand(1, 10)
        ],
            [
                'purchase_date' => Carbon::create('2022', '6', '8'),
                'price' => 12600,
                'discount' => 0,
                'stage_id' => rand(1, 6),
                'user_id' => rand(1, 10)
            ],
            [
                'purchase_date' => Carbon::create('2022', '6', '12'),
                'price' => 10000,
                'discount' => 0,
                'stage_id' => rand(1, 6),
                'user_id' => rand(1, 10)
            ],
            [
                'purchase_date' => Carbon::create('2022', '5', '21'),
                'price' => 7000,
                'discount' => 0,
                'stage_id' => rand(1, 6),
                'user_id' => rand(1, 10)
            ],
        ]);
    }
}
