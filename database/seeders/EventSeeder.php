<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([[
            'name' => "Boris Brejcha",
            'start' => Carbon::create('2022', '7', '20'),
            'stage_id' => rand(1, 6),
            'user_id' => rand(1, 10)
        ],
            [
                'name' => "Nick Cave and the Bad Seeds",
                'start' => Carbon::create('2022', '7', '21'),
                'stage_id' => rand(1, 6),
                'user_id' => rand(1, 10)
            ]]);
    }
}
