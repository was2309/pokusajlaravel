<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        DB::table('stages')->insert([[
            'name' => "Main Stage",
            'location' => "Petrovaradin: center",
            'sponsor' => "Exit",
            'capacity' => random_int(5000, 15000)
        ],
            [
                'name' => "Dance Arena",
                'location' => "Petrovaradin: east",
                'sponsor' => "mts",
                'capacity' => random_int(5000, 15000)
            ],
            [
                'name' => "Fusion Stage",
                'location' => "Petrovaradin: west",
                'sponsor' => "VISA",
                'capacity' => random_int(5000, 15000)
            ],
            [
                'name' => "Explosive Stage",
                'location' => "Petrovaradin: south",
                'sponsor' => "Explosive pub",
                'capacity' => random_int(5000, 15000)
            ],
            [
                'name' => "No Sleep Stage",
                'location' => "Petrovaradin: north",
                'sponsor' => "Guarana",
                'capacity' => random_int(5000, 15000)
            ],
            [
                'name' => "Gang Beats Stage",
                'location' => "Petrovaradin: north-east",
                'sponsor' => "Noizz",
                'capacity' => random_int(5000, 15000)
            ]]);
    }
}
