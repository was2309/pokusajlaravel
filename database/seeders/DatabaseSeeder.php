<?php

namespace Database\Seeders;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::factory(10)->create();
        $this->call(StageSeeder::class);
        $this->call(PerformerSeeder::class);
        $this->call(EventSeeder::class);
        $this->call(EventPerformerSeeder::class);
//        $this->call(TicketSeeder::class);
        Ticket::factory(10)->create();
    }
}
