<?php

namespace Database\Seeders;

use App\Models\Performer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PerformerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('performers')->insert(Performer::factory(15)->create()->get());
        Performer::factory(15)->create();
    }
}
