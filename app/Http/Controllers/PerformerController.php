<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Http\Resources\PerformerResource;
use App\Http\Resources\StageResource;
use App\Models\Event;
use App\Models\Performer;
use App\Models\Stage;
use Illuminate\Http\Request;
use PHPUnit\Util\Exception;

class PerformerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $performers = Performer::all();
        return response()->json(PerformerResource::collection($performers));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'nick' => 'required',
        ]);

        $performer = (new Performer())->create([
            'name' => $request->name,
            'surname'=>$request->surname,
            'nick'=>$request->nick,
            'music_genre' => $request->music_genre
        ]);
        return response()->json($performer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
//            $event = Performer::with(['name', 'surname', 'nick', 'music_genre'])->findOrFail($id); //eager loading
            $event = Performer::findOrFail($id); //eager loading
            return response()->json(new PerformerResource($event));
        }catch (Exception $exception){
            return response()->json(['exception' => $exception], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\Response
     */
    public function edit(Performer $performer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Performer $performer)
    {
        $request->validate([
            'nick'=>'required',

        ]);

//        $event = (new Event)->findOrFail($id);

        $status = $performer->update([
            'name' => $request->name,
            'surname'=>$request->surname,
            'nick'=>$request->nick,
            'music_genre' => $request->music_genre
        ]);

        return response()->json($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Performer $performer)
    {
        $status = $performer->delete();
        return response()->json($status);
    }
}
