<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Models\Event;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index(): string
    {
        $events = Event::all();
        return response()->json(EventResource::collection($events));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:App\Models\Event,name',
            'start' => 'required',
            'stage_id' => 'required',
            'user_id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $event = Event::create($request->all());
        return response()->json(EventResource::make($event));
    }

    /**
     * Display the specified resource.
     *
* //     * @param  \App\Models\Event  $event
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        try {
            $event = Event::with(['stage', 'performers', 'user'])->findOrFail($id);
            return response()->json(new EventResource($event));
        }catch (Exception $exception){
            dd($exception);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Event $event
     * @return Response
     */
    public function edit(Event $event)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
//        $validator = $request->validate([
//            'name' => 'required|unique',
//            'start' => 'required',
//            'stage_id' => 'required',
//            'user_id' => 'required'
//        ]);

        $validator = Validator::make($request->all(), [
//            'name' => 'required|unique:App\Models\Event,name',
            'name' => 'required',
            'start' => 'required',
            'stage_id' => 'required',
            'user_id' => 'required',
            'created_at' => '',
            'updated_at' => ''
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $event = Event::with(['stage', 'performers', 'user'])->findOrFail($id);

        $status = $event->update([
            'name' => $request->get('name'),
            'start'=>$request->input('start'),
            'stage_id'=>$request->get('stage_id'),
            'user_id' => $request->get('user_id')
        ]);

//        $status = $event->update($request->all());

        return response()->json($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        // da bismo obrisali event moramo da obrisemo prvo njegovu vezu sa izvodjacem koja se nalazi u pivot tabeli
        $event = Event::with(['stage', 'performers', 'user'])->findOrFail($id);


        $event_performer = DB::table('event_performer')->where('event_id', $event->id)->delete();


        $status = $event->delete();
        return response()->json($status);
    }
}
