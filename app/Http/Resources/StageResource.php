<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StageResource extends JsonResource
{

    public static $wrap = 'stage';

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        return parent::toArray($request);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'location' => $this->start,
            'sponsor' => $this->sponsor,
            'capacity' => $this->capacity,
//            'events' => EventResource::collection($this->events) ovo blokira aplikaciju jer se ulazi u beskonačnu rekurziju
        ];
    }
}
