<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\PerformerController;
use App\Http\Controllers\StageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('events', [EventController::class, 'index']); //radi

Route::get('stages', [StageController::class, 'index']); //radi

Route::get('performers', [PerformerController::class, 'index']); //radi

Route::get('events/{id}', [EventController::class, 'show']); //radi



Route::post('register', [AuthController::class, 'register']); // radi, uvek osvezi bazu

Route::post('/login', [AuthController::class, 'login']); // radi

Route::group(['middleware' => ['auth:sanctum']], function () { // radi
    Route::get('/profile', function (Request $request) {
        return auth()->user();
    });

    Route::post('events', [EventController::class, 'store']); //radi, samo mora da se pazi na ogranicenja pri unosu

    Route::put('events/{id}', [EventController::class, 'update']); // radi ali ne mogu da se prosledjuju podaci kroz formu vec mora bas kao JSON

    Route::delete('events/{id}', [EventController::class, 'destroy']);  // radi

    Route::post('/logout', [AuthController::class, 'logout']); // radiii
});
